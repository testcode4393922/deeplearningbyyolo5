import torch
import torchvision

print(torch.version.cuda)
print(torch.__version__)
print(torchvision.__version__)
print(torch.cuda.is_available())

if torch.cuda.is_available():
    num_gpus = torch.cuda.device_count()
    for i in range(num_gpus):
        print(f'Device {i}: {torch.cuda.get_device_name(i)}')
else:
    print('No CUDA-enabled GPU found.')
