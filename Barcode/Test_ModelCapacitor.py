import torch

# กำหนดอุปกรณ์ให้เป็น GPU
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Model
model = torch.hub.load("ultralytics/yolov5", "custom", "../Model_Capacitor.pt").to(device)

print(f'Model is using device: {next(model.parameters()).device}')

# Images
img = "TestCapacitor9.jpg"  # or file, Path, PIL, OpenCV, numpy, list

# Inference
results = model(img)

# Results
results.show()  # or .show(), .save(), .crop(), .pandas(), etc.



# CPU
# python train.py --img 640 --epochs 3 --data data.yaml --weights yolov5s.pt

# GPU
# python train.py --img 640 --epochs 3 --data data.yaml --weights yolov5s.pt --device 0