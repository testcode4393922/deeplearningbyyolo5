import torch

# กำหนดอุปกรณ์ให้เป็น GPU
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Model
model = torch.hub.load('ultralytics/yolov5', 'yolov5s', pretrained=True).to(device) # or yolov5n - yolov5x6, custom

print(f'Model is using device: {next(model.parameters()).device}')

# Images
img = "Test_yolo5s.jpg"  # or file, Path, PIL, OpenCV, numpy, list

# Inference
results = model(img)

# Results
results.show()  # or .show(), .save(), .crop(), .pandas(), etc.